#!/usr/bin/env python3
from functools import wraps


def make_torta(filling):
    @wraps(filling)
    def wrapper(*args, **kwargs):
        print('/--bolillo--\\')
        filling()
        print('\--bolillo--/')
    return wrapper


def make_tostada(filling):
    @wraps(filling)
    def wrapper(*args, **kwargs):
        filling()
        print('--tortilla--')
    return wrapper


def make_quesadilla(filling):
    @wraps(filling)
    def wrapper(*args, **kwargs):
        print('--tortilla--')
        print('---queso----')
        filling()
        print('---queso----')
        print('--tortilla--')
    return wrapper


def make_sandwich(filling):
    @wraps(filling)
    def wrapper(*args, **kwargs):
        print('|--bread--|')
        filling()
        print('|--bread--|')
    return wrapper


@make_torta
def fajita_torta():
    print('peppers')
    print('onions')


@make_quesadilla
def fajita_quesadilla():
    print('peppers')
    print('onions')


@make_sandwich
def pbj():
    print('peanut butter')
    print('jelly')


if __name__ == "__main__":
    pbj()
    fajita_torta()
    fajita_quesadilla()
