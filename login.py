#!/usr/bin/env python3
from functools import wraps


def redirect():
    """redirects the page."""
    pass


def load_secret_data():
    """loads secret data"""
    return 'secret'


def load(secret):
    return secret


def delete(secret):
    return secret


def create(secret):
    return secret


def load_page():
    pass


class User:
    """user class"""
    def is_logged_in(self):
        """checks if user is logged in."""
        return False


user = User()


def login_required(f):
    """login required decorator"""
    @wraps(f)
    def wrapper():
        """is a wrapper"""
        if not user.is_logged_in():
            raise 'NotAuthorized'
        f()
    return wrapper


@login_required
def load_secret_page():
    """Loads a secret page"""
    secret = load_secret_data()
    return load_page(secret)


def load_secret():
    if not user.is_logged_in():
        raise 'NotAuthorized'

    secret = load_secret_data()
    return load(secret)


def delete_secret():
    if not user.is_logged_in():
        raise 'NotAuthorized'

    secret = load_secret_data()
    return delete(secret)


def create_secret():
    if not user.is_logged_in():
        raise 'NotAuthorized'

    return create('this part is secret')


if __name__ == "__main__":
    # print(load_secret_page.__name__)
    # print(load_secret_page.__doc__)
    # load_secret()
    # delete_secret()
    create_secret()

