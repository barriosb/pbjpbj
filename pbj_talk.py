#!/usr/bin/env python3
from dis import dis


def decor(funca):
    """This is a decorator function."""

    def wrapper():
        """This is a wrapper function."""
        print('before')
        funca()
        print('after')

    return wrapper


def some_func():
    print('func')


@decor
def decor_func():
    """THis is a function with a decorator."""
    print('func')


if __name__ == "__main__":
    d = decor(some_func)
    # print(d)
    dis(d)
    dis(decor_func)
    # decor_func()
    # print(decor_func.__name__)
    # print(decor_func.__doc__)
