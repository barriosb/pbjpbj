#!/usr/bin/env python3
from time import time, sleep
from functools import wraps
import logging
import sys


def timer(func):
    @wraps(func)
    def wrapper():
        begin = time()
        return_value = func()
        print((time() - begin) * 1000)
        return return_value
    return wrapper


@timer
def func_tester():
    sleep(1)


def memoize(func):
    memo = {}
    @wraps(func)
    def wrapper(num):
        if num not in memo:
            memo[num] = func(num)
        return memo[num]
    return wrapper


@memoize
def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)


FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(stream=sys.stdout, format=FORMAT)
logger = logging.getLogger('myapp')
logger.setLevel(logging.DEBUG)


def log_it(func):
    @wraps(func)
    def wrapper():
        logging.info('hi')
        logger.info(f"{func.__name__}")
    return wrapper


@log_it
def some_funcer():
    print('hi')


def login_required(func):
    @wraps(func)
    def wrapper():
        # if not user.logged_in():
        if True:
            raise 'NotAuthorized'
        func()
    return wrapper


@login_required
def secret_page():
    print('hi')


if __name__ == "__main__":
    func_tester()
    print(fib(100))
    some_funcer()
    secret_page()
